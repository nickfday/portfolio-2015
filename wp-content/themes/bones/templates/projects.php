<div class="container" ng-controller="slick-example">
  <div class="row project" ng-show="shown">
    <button ng-click="close()" id="close" type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div ng-show="selectedTest != null" animate-show ng-animate="'fade'">
      <h2 class="pull-left">{{selectedTest.parent.title}}</h2>
      <img class="logo pull-right" src="{{selectedTest.logo}}">
      <h5>{{selectedTest.location}}</h5>
      <?php $data =  json_decode($json);?>
      <p ng-bind-html="selectedTest.parent.content"></p>
      <div class="col-sm-3">
        <img src="{{selectedTest.screenShot}}">
      </div>
      <div class="col-sm-8 col-sm-offset-1">
        <p class="intro">{{selectedTest.intro}}</p>
        <p>{{selectedTest.body}}</p>
      </div>
    </div>
    <button ng-click="previous()">Prev</button>
    <button ng-click="next()">Next</button>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <h2>Projects</h2>
      <slick dots=true infinite=false speed=300 slides-to-show=1 touch-move=false slides-to-scroll=1 init-onload="true" data="projects">
        <div ng-repeat="project in projects" ng-click='test(project)'>
          <img data-lazy="{{project.source}}">
          <div class="slide__caption">{{project.parent.title}}</div>
        </div>
      </slick>
    </div>
  </div>
</div>