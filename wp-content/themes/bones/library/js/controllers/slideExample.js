/**
 * Created by nickf on 18/06/15.
 */

var app = angular.module('myApp', ['slick']);

app.controller('slick-example', [$scope, $http, $element], function ($scope, $http, $element) {
    $http.get("/wp-json/media?parent?type=project")
        .success(function (response) {
            $scope.projects = response;
        });
    $scope.selectedTest = null;
    $scope.shown = false;
    $scope.dataLoaded = true;

    $scope.test = function (project) {
        console.log(project);
        $scope.selectedTest = project;
        $scope.shown = true;
        window.scrollTo(1, 1);
        var currentSlide = $('.slick-slider').slick('slickCurrentSlide');

    }

    $scope.close = function () {
        $scope.shown = false;
    }
    $scope.next = function () {
        console.log($scope.selectedTest.$$hashKey);
        var slide = angular.element.find('.carousel .slide')[2];
        //console.log(slide);
        for(var i=0; i<5; i++)
            slide.click();
    }

    $scope.previous = function () {
        window.alert('prev');
    }

});