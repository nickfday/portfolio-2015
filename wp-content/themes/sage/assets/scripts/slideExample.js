/**
 * Created by nickf on 18/06/15.
 */


var myApp = angular.module('myApp',[]);
myApp.requires.push('slick');
myApp.requires.push('ngSanitize');
myApp.requires.push('angular-loading-bar');
//myApp.requires.push('ngAnimate');


myApp.service('projectService', ['$http', '$q', function($http, $q){
    var deferred = $q.defer();
    $http.get("/wp-json/media?parent?type=project").then(function(data) {
     deferred.resolve(data);
    });
    this.getProjects = function() {
        return deferred.promise;
    };
}]);

myApp.controller('ProjectController', ['$scope','projectService', function($scope, projectService) {
    var promise = projectService.getProjects();
    promise.then(function (data) {
        $scope.projects = data.data;
    });
    $scope.test = function (project) {
        $scope.selectedTest = project;
    };
    $scope.close = function() {
        $scope.shown = false;
    };

    $scope.selectedTest = null;
    $scope.shown = false;
    $scope.dataLoaded = true;

    $scope.test = function (project) {
        $scope.selectedTest = project;
        $scope.shown = true;
        //window.location.href = '/#projects';
        //var currentSlide = $('.slick-slider').slick('slickCurrentSlide');

    };

}]);

myApp.controller('aboutController', ['$scope', function($scope){
    $http.get("/wp-json/posts").then(function(data) {
        deferred.resolve(data);
    });
}]);