<?php
/**
 * Template Name: Custom Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>

<?php include 'templates/intro.php'?>
<?php include 'templates/carousel.php'?>
<?php include 'templates/about.php'?>



  <div id="contact" class="row section">
    <div class="container">
        <h2 class="text-center">Contact</h2>
        <div class="col-sm-6">
          <?php echo do_shortcode( '[contact-form-7 id="71" title="Contact form 1"]' ); ?>
        </div>
        <div class="col-sm-6">
          <?php echo do_shortcode( '[wpgmza id="1"]' ); ?>
        </div>
    </div>
  </div>




