<div id="about" class="bleed row section " style="background: #f5f5f5">
<div  class="container">
  <div class="sm-col-12">
    <h2 class="text-center">About</h2>
    <p>Finley-Day Limited are passionate about everything digital. We specialise in building web applications that work beautifully in any device. We embrace open source technology. Drupal and Wordpress
    are our content management systems of choice.
    </p>
  </div>

</div>
  </div>