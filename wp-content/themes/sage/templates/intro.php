<div id="intro" class="row section shadow" style="background: #f5f5f5">
  <div class="container">
  <div class="col-sm-4">
    <h2 class="text-center">Welcome</h2>
    <p>We build professional web applications. From small-scale to large enterprise solutions. <a href="#about">Read more about us</a>.</p>
  </div>
  <div class="col-sm-4">
    <h2 class="text-center">Key Skills</h2>
    <ul class="col-xs-6">
        <li>PHP</li>
        <li>Responsive Design</li>
        <li>GIT</li>
    </ul>
    <ul class="col-xs-6">
        <li>Wordpress</li>
        <li>Drupal 7</li>
        <li>Javascript</li>
    </ul>
  </div>
  <div class="col-sm-4 xs-clear">
    <h2 class="text-center">Contact</h2>
    <p>If you're interested in our services please <a href="#contact">contact us</a>.</p>
  </div>
    </div>
</div>