<div id="projects" class="wrapper section">
  <div class="container">
    <h2 class="text-center">Projects</h2>
    <div ng-controller="ProjectController">



      <div class="row">
        <div class="col-sm-12">
          <slick dots=true infinite=false speed=300 arrows=true slides-to-show=6 touch-move=false slides-to-scroll=1 init-onload="true" data="projects">
            <div ng-repeat="project in projects | orderBy:'parent.date':true" ng-click='test(project)'>
              <img data-lazy="{{project.source}}">
              <div class="slide__caption">{{project.parent.title}}</div>
            </div>
          </slick>
        </div>
      </div>

      <div id="single-project">
      <div class="row project well" ng-show="shown">
        <button ng-click="close()" id="close" type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div ng-show="selectedTest != null" animate-show ng-animate="'fade'">
          <h3 class="title">{{selectedTest.parent.title}}</h3>
          <div ng-bind-html="selectedTest.parent.content"></div>
        </div>
        <!--        <button ng-click="previous()">Prev</button>-->
        <!--        <button ng-click="next()">Next</button>-->
      </div>
      </div>

    </div>
  </div>
</div>
