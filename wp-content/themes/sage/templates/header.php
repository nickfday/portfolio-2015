<header class="banner" role="banner">
  <div class="container">
<!--    <a class="brand" href="--><?//= esc_url(home_url('/')); ?><!--">--><?php //bloginfo('name'); ?><!--</a>-->
<!--    <nav role="navigation">-->
<!--      --><?php
//      if (has_nav_menu('primary_navigation')) :
//        wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav']);
//      endif;
//      ?>
<!--    </nav>-->
    <nav class="navbar navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div class="brand" href="<?= esc_url(home_url('/')); ?>"><span class="company">Finley-Day Limited</span><span class="slogan">Professional Web Development</span></div>
        </div>
        <div id="navbar" class="navbar-collapse collapse navbar-right">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#intro">Intro</a></li>
            <li><a href="#projects">Projects</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
<!--          <ul class="social-nav">-->
<!--            <li height="" style="margin-top:13px;git "><a href="https://uk.linkedin.com/pub/nicholas-finley-day/25/3b8/454" style="text-decoration:none;"><span style="font: 80% Arial,sans-serif; color:#fff;padding-top:10px"><img src="https://static.licdn.com/scds/common/u/img/webpromo/btn_in_20x15.png" width="20" height="15" alt="View my LinkedIn profile" style="vertical-align:middle;" border="0">&nbsp;View my LinkedIn</span></a></li>-->
<!--            <li height="30px">-->
<!--              <a href="https://twitter.com/nickfday" class="twitter-follow-button" data-show-count="false">Follow @nickfday</a>-->
<!--              <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>-->
<!--            </li>-->
<!--          </ul>-->
        </div><!--/.nav-collapse -->
      </div>
    </nav>
  </div>
</header>
